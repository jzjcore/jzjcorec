# JZJCoreC

My third RISC-V soft core cpu implementation (RV32IZifencei) with even lower cycles per instruction and an even higher fMax.

My EP4CE6E22C8N has an fMax of over 70 mhz at 0C with this design, and the slower model still has an fmax around 30mhz!

This cpu is 90% JZJCoreB (which was in turn 80% JZJCoreA). The main things that are different now are memory (dual read ports: one for instructions, one for data; and many critical fixes to MemoryController, BitExtensionFunctions and MemoryMappedIOManager) and control (fetches occur during instructions instead of after which reduces CPI by 1 from JZJCoreB for all instructions after the first). Despite the small changes, going from B to C was roughly as big an improvement as the jump from A to B in terms of percentage of a performance improvement (for the slow model including fmax and cpi).

I think true pipelining (not just fetching instructions ahead of time) will be a thing for JZJCoreD.

## Cycle counts for instructions

Note: The first instruction executed takes an additional cycle because it needs to be fetched from memory. After that, the next instruction is fetched during the execution of the one that was previously fetched.

Some instructions take two cycles so the fetch for the next instruction is performed on the second cycle of the two cycle instruction.

| Instruction | Cycle Count |
|:------------|:-----------------------|
|-Base Spec(I)-|
| lui | 1 |
| auipc | 1 |
| jal | 1 |
| jalr | 1 |
| beq | 1 |
| bne | 1 |
| blt | 1 |
| bge | 1 |
| bltu | 1 |
| bgeu | 1 |
| lb | 2 |
| lh | 2 |
| lw | 2 |
| lbu | 2 |
| lhu | 2 |
| sb | 2 |
| sh | 2 |
| sw | 1 |
| addi | 1 |
| slti | 1 |
| sltiu | 1 |
| xori | 1 |
| ori | 1 |
| andi | 1 |
| slli | 1 |
| srli | 1 |
| srai | 1 |
| add | 1 |
| sub | 1 |
| sll | 1 |
| slt | 1 |
| sltu | 1 |
| xor | 1 |
| srl | 1 |
| sra | 1 |
| or | 1 |
| and | 1 |
| fence | 1 |
| ecall | 1 |
| ebreak | 1 |
|-Zifencei-|
| fence.i | 1 |

## Memory Map

Note: addresses are inclusive, bounds not specified are not addressed to anything, and execution starts at 0x00000000

| Bytewise Address (whole word) | Physical Word-wise Address | Function |
|:------------------------------|:---------------------------|:---------|
|0x00000000 to 0x00000003|0x00000000|RAM Start|
|0x0000FFFC to 0x0000FFFF|0x00003fff|RAM End|
|0xFFFFFFE0 to 0xFFFFFFE3|0x3FFFFFF8|Memory Mapped IO Registers Start|
|0xFFFFFFFC to 0xFFFFFFFF|0x3FFFFFFF|Memory Mapped IO Registers End|
