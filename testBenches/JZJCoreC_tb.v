`timescale 1ns/1ps
module JZJCoreC_tb;

reg clock = 1'b0;

JZJCoreC #(.INITIAL_MEM_CONTENTS("../src/memFiles/fibbonaccijalr.mem")) coreTest (.clock(clock), .reset(1'b0));

always
begin
    #10;
    clock = ~clock;
end

initial
begin
    $dumpfile("/tmp/JZJCoreC.vcd");
    $dumpvars(0,JZJCoreC_tb);
    #10000 $finish;
end

endmodule
