cd ../src
iverilog -o /tmp/JZJCoreC.vvp ../testBenches/JZJCoreC_tb.v JZJCoreC/ALU.v JZJCoreC/ControlLogic.v JZJCoreC/InstructionDecoder.v JZJCoreC/JZJCoreC.v JZJCoreC/PCALU.v JZJCoreC/ProgramCounter.v JZJCoreC/RegisterFile.v JZJCoreC/ValueFormer.v JZJCoreC/Memory/MemoryBackend.v JZJCoreC/Memory/MemoryController.v JZJCoreC/Memory/TriplePortSynchronousRam.v JZJCoreC/Memory/MemoryMappedIOManager.v JZJCoreC/Memory/RAMWrapper.v
vvp /tmp/JZJCoreC.vvp
gtkwave /tmp/JZJCoreC.vcd
