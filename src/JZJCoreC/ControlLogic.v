module ControlLogic
(
	input clock,
	input reset,
	
	//Control lines
	output reg registerFileWE,//Latch new data into register rd at next posedge
	//ALU
	output reg aluOE,//Output alu result to registerFileIn
	output reg opImm,//Whether the opcode type is opImm (1) or op (0)
	//Value former
	output reg valueFormerOE,//Output formed value to registerFileIn
	output reg valueFormerOperation,//Whether to output the value for a lui (0) is auipc (1) instruction
	//Program counter
	output reg pcWE,
	//Program counter ALU
	output reg pcALUOE,
	output reg [1:0] pcALUOperation,
	//Memory
	output reg memoryEnable,
	output reg [1:0] memoryOperation,
	
	//Error flags
	input pcMisaligned,//program counter is not aligned to a 4 byte address
	input jumpMisaligned,//new program counter address is not aligned to a 4 byte address
	input memoryAccessMisaligned,//memory access is not aligned to a 4 byte address for a word or a 2 byte address for a half word
	input badInstruction,//bad encoding (funct3, funct7)
	
	//Instruction stuffs
	input [31:0] instructionIn,//Instruction fetch from memory module
	output reg [31:0] instructionAddressToAccess,//Contains address to access to get an instruction from instructionIn
	output [31:0] instructionOutNoOpcode,//Instruction out (except for opcode) to be decoded by decoder module
	input [2:0] funct3,//from decoder module
	input [31:0] pcIn,
	input [31:0] nextPC
);
/* Wires, Registers, Assignments, and Constants */

//State stuff (one hot encoding)
localparam STATE_INITIAL_IFETCH = 4'b0001;
localparam STATE_EXECUTE_IFETCH = 4'b0010;
localparam STATE_EXECUTE = 4'b0100;//1st cycle of a 2 cycle instruction
localparam STATE_HALT = 4'b1000;

reg [3:0] currentState = STATE_INITIAL_IFETCH;
reg [3:0] nextState;
reg canContinueFetching;//0 if the instruction needs to delay another fetch to do what it needs to do
reg halt = 1'b0;//next state should be STATE_HALT and the cpu should halt execution

//Initial negedge of fpga seems to cause problems because state is changed from STATE_INITIAL_IFETCH immediatly instead of waiting a clock cycle; this is used to not change state the first negedge
reg initialDelayRegister = 1'b0;

//Instruction in holds instruction currently being executed, and is updated based on instructionAddressToAccess on each posedge (thanks to output registers present in sram)
//No need for instruction register this way
assign instructionOutNoOpcode = {instructionIn[31:7], 7'b0000000};
wire [6:0] opcode = instructionIn[6:0];

/* State Logic */

//Change state each negedge
always @(negedge clock, posedge reset)
begin
	if (reset)
		currentState <= STATE_INITIAL_IFETCH;
	else if (~clock)
	begin
		if (initialDelayRegister)
			currentState <= nextState;//current_state becomes new_state each clock pulse
		else//Initial negedge of fpga seems to cause problems because state is changed from STATE_INITIAL_IFETCH immediatly instead of waiting a clock cycle; this is used to not change state the first negedge
			initialDelayRegister <= 1'b1;//Waited a the first negedge; now we can begin execution
	end
end

//Decide next state
always @*
begin
	if (halt || pcMisaligned || jumpMisaligned || memoryAccessMisaligned || badInstruction)
		nextState = STATE_HALT;
	else//we're not halting the cpu
	begin
		case (currentState)
			STATE_INITIAL_IFETCH:
			begin
				if (canContinueFetching)//instructionIn indicates a 1 cycle instruction
					nextState = STATE_EXECUTE_IFETCH;
				else//2 cycle instruction
					nextState = STATE_EXECUTE;//Can't fetch again immediatly; instruction needs more time
			end
			STATE_EXECUTE_IFETCH:
			begin
				if (canContinueFetching)//instructionIn indicates a 1 cycle instruction
					nextState = STATE_EXECUTE_IFETCH;
				else//2 cycle instruction
					nextState = STATE_EXECUTE;
			end
			STATE_EXECUTE: nextState = STATE_EXECUTE_IFETCH;//1st cycle done, 2nd cycle behaves just like STATE_EXECUTE_IFETCH
			STATE_HALT: nextState = STATE_HALT;//spin forever
		endcase
	end
end

//Decide if canContinueFetching (after the posedge of an instruction latch into instruction register)
always @*
begin
	if ((currentState == STATE_INITIAL_IFETCH) || (currentState == STATE_EXECUTE_IFETCH))
	begin
		case (opcode)
			7'b0010011: canContinueFetching = 1'b1;//alu immediate operation
			7'b0110011: canContinueFetching = 1'b1;//alu register-register operation (from base spec or m extension)
			7'b0000011: canContinueFetching = 1'b0;//memory read (1 clock to read from sram, 1 clock to store to reg)
			7'b0100011://memory write
			begin
				if (funct3 == 3'b010)
					canContinueFetching = 1'b1;//sw (though this means zifencei is not supported)
				else
					canContinueFetching = 1'b0;//sb or sh because of read+modify-write cycle
			end
			7'b0110111: canContinueFetching =  1'b1;//lui
			7'b0010111: canContinueFetching =  1'b1;//auipc
			7'b0001111: canContinueFetching =  1'b1;//fence/fence.i
			7'b1110011: canContinueFetching =  1'b1;//ecall/ebreak
			7'b1101111: canContinueFetching =  1'b1;//jal
			7'b1100111: canContinueFetching =  1'b1;//jalr
			7'b1100011: canContinueFetching =  1'b1;//branch instructions
			default: canContinueFetching =  1'b1;//if we have a bad opcode, we halt so the value of this dosen't matter
		endcase
	end
	else//STATE_EXECUTE (technically applies to STATE_HALT too but this this does not matter for STATE_HALT)
	begin
		case (opcode)
			7'b0000011: canContinueFetching = 1'b1;//memory read (1 clock to read from sram, 1 clock to store to reg)
			7'b0100011://memory write
			begin
				if (funct3 == 3'b010)
					canContinueFetching = 1'b1;//should not reach here with sw
				else
					canContinueFetching = 1'b1;//sb or sh can finaly fetch; write part of read+modify-write cycle
			end
			default: canContinueFetching =  1'b1;//should never get here for any other opcode
		endcase
	end
end

/* Instruction Fetch Logic */

//Output proper instructionAddressToAccess
always @*
begin
	if ((currentState == STATE_INITIAL_IFETCH) || (currentState == STATE_EXECUTE))
		instructionAddressToAccess = pcIn;//Either the cpu is just starting to execute its first instruction or we are in the middle of a 2 cycle instruction
	else//STATE_EXECUTE_IFETCH (technically applies to STATE_HALT too but this this does not matter for STATE_HALT)
		instructionAddressToAccess = nextPC;//Can look ahead at the next instruction becuase it only takes 1 cycle
end

/* Control Signal Logic */

always @*
begin
	case (currentState)
		STATE_INITIAL_IFETCH:
		begin
			//There is no actual need to do anything because instructionIn is always active and the address to access is handled by instruction fetch logic
			registerFileWE = 1'b0;
			aluOE = 1'b0;
			opImm = 1'b0;
			valueFormerOE = 1'b0;
			valueFormerOperation = 1'b0;
			pcWE = 1'b0;
			pcALUOE = 1'b0;
			pcALUOperation = 2'b00;
			memoryEnable = 1'b0;
			memoryOperation = 2'b00;
			halt = 1'b0;
		end
		STATE_EXECUTE_IFETCH:
		begin
			case (opcode)
				7'b0010011://alu immediate operation
				begin
					valueFormerOE = 1'b0;
					valueFormerOperation = 1'b0;
					pcALUOE = 1'b0;
					pcALUOperation = 2'b00;
					memoryEnable = 1'b0;
					memoryOperation = 2'b00;
					halt = 1'b0;
					
					aluOE = 1'b1;//Get result from alu
					opImm = 1'b1;//The opcode type is OP-IMM
					registerFileWE = 1'b1;//Store in rd
					
					pcALUOE = 1'b1;//Enable pc alu
					pcALUOperation = 2'b11;//increment pc (won't write to register file bus)
					pcWE = 1'b1;//store new pc
				end
				7'b0110011://alu register-register operation (from base spec or m extension)
				begin
					valueFormerOE = 1'b0;
					valueFormerOperation = 1'b0;
					memoryEnable = 1'b0;
					memoryOperation = 2'b00;
					halt = 1'b0;
					
					aluOE = 1'b1;//Get result from alu
					opImm = 1'b0;//The opcode type is OP
					registerFileWE = 1'b1;//Store in rd
					
					pcALUOE = 1'b1;//Enable pc alu
					pcALUOperation = 2'b11;//increment pc (won't write to register file bus)
					pcWE = 1'b1;//store new pc
				end
				7'b0000011://memory read
				begin//This is done second
					aluOE = 1'b0;
					opImm = 1'b0;
					valueFormerOE = 1'b0;
					valueFormerOperation = 1'b0;
					halt = 1'b0;
					
					registerFileWE = 1'b1;//now we can latch the data into rd
					memoryEnable = 1'b1;//Leave memory enabled so it continues to output its value until the posedge
					memoryOperation = 2'b00;//load instruction
					
					//Now the pc can be incremented
					pcALUOE = 1'b1;//Enable pc alu
					pcALUOperation = 2'b11;//increment pc (won't write to register file bus)
					pcWE = 1'b1;//store new pc
				end
				7'b0100011://memory write
				begin//This happens second (first and only for sw)
					registerFileWE = 1'b0;
					aluOE = 1'b0;
					opImm = 1'b0;
					valueFormerOE = 1'b0;
					valueFormerOperation = 1'b0;
					halt = 1'b0;
					
					memoryEnable = 1'b1;//Enable memory
					memoryOperation = 2'b10;//Now we can write the modified word back to memory or in the case of sw, the entire new word will be written
					
					//Now the pc can be incremented
					pcALUOE = 1'b1;//Enable pc alu
					pcALUOperation = 2'b11;//increment pc (won't write to register file bus)
					pcWE = 1'b1;//store new pc
				end
				7'b0110111://lui
				begin
					aluOE = 1'b0;
					opImm = 1'b0;
					memoryEnable = 1'b0;
					memoryOperation = 2'b00;
					halt = 1'b0;
					
					registerFileWE = 1'b1;//Write upper immediate to rd
					valueFormerOE = 1'b1;//Get value from value former
					valueFormerOperation = 1'b0;//get lui value
					
					pcALUOE = 1'b1;//Enable pc alu
					pcALUOperation = 2'b11;//increment pc (won't write to register file bus)
					pcWE = 1'b1;//store new pc
				end
				7'b0010111://auipc
				begin
					aluOE = 1'b0;
					opImm = 1'b0;
					memoryEnable = 1'b0;
					memoryOperation = 2'b00;
					halt = 1'b0;
					
					registerFileWE = 1'b1;//Write upper immediate to rd
					valueFormerOE = 1'b1;//Get value from value former
					valueFormerOperation = 1'b1;//get auipc value
					
					pcALUOE = 1'b1;//Enable pc alu
					pcALUOperation = 2'b11;//increment pc (won't write to register file bus)
					pcWE = 1'b1;//store new pc
				end
				7'b0001111://fence/fence.i
				begin
					//do nothing (waiting after a write for an instruction so the fetch during this instruction's execution pulls from updated memory)
					registerFileWE = 1'b0;
					aluOE = 1'b0;
					opImm = 1'b0;
					valueFormerOE = 1'b0;
					valueFormerOperation = 1'b0;
					memoryEnable = 1'b0;
					memoryOperation = 2'b00;
					halt = 1'b0;
					
					pcALUOE = 1'b1;//Enable pc alu
					pcALUOperation = 2'b11;//increment pc (won't write to register file bus)
					pcWE = 1'b1;//store new pc
				end
				7'b1110011://ecall/ebreak
				begin
					registerFileWE = 1'b0;
					aluOE = 1'b0;
					opImm = 1'b0;
					valueFormerOE = 1'b0;
					valueFormerOperation = 1'b0;
					pcWE = 1'b0;
					pcALUOE = 1'b0;
					pcALUOperation = 2'b00;
					memoryEnable = 1'b0;
					memoryOperation = 2'b00;
					
					halt = 1'b1;//ecalls/ebreaks just cause a fatal trap intentionally
				end
				7'b1101111://jal
				begin
					aluOE = 1'b0;
					opImm = 1'b0;
					valueFormerOE = 1'b0;
					valueFormerOperation = 1'b0;
					memoryEnable = 1'b0;
					memoryOperation = 2'b00;
					halt = 1'b0;
					
					pcALUOE = 1'b1;//Enable pc alu
					pcALUOperation = 2'b00;//jal
					registerFileWE = 1'b1;//Store new rd value
					pcWE = 1'b1;//Store new pc value
				end
				7'b1100111://jalr
				begin
					aluOE = 1'b0;
					opImm = 1'b0;
					valueFormerOE = 1'b0;
					valueFormerOperation = 1'b0;
					memoryEnable = 1'b0;
					memoryOperation = 2'b00;
					halt = 1'b0;
					
					pcALUOE = 1'b1;//Enable pc alu
					pcALUOperation = 2'b01;//jalr
					registerFileWE = 1'b1;//Store new rd value
					pcWE = 1'b1;//Store new pc value
				end
				7'b1100011://branch instructions			
				begin
					aluOE = 1'b0;
					opImm = 1'b0;
					valueFormerOE = 1'b0;
					valueFormerOperation = 1'b0;
					memoryEnable = 1'b0;
					memoryOperation = 2'b00;
					halt = 1'b0;
					
					pcALUOE = 1'b1;//Enable pc alu
					pcALUOperation = 2'b10;//branch instruction
					registerFileWE = 1'b0;//Branch instructions do not have an rd to store
					pcWE = 1'b1;//Store new pc value
				end
				default://Bad opcode
				begin
					//Disable everything
					registerFileWE = 1'b0;
					aluOE = 1'b0;
					opImm = 1'b0;
					valueFormerOE = 1'b0;
					valueFormerOperation = 1'b0;
					pcWE = 1'b0;
					pcALUOE = 1'b0;
					pcALUOperation = 2'b00;
					memoryEnable = 1'b0;
					memoryOperation = 2'b00;
					
					halt = 1'b1;//Bad opcode
				end
			endcase
		end
		STATE_EXECUTE://This happens first for the instructions that use it
		begin
			case (opcode)
				7'b0000011://memory read
				begin//This is done first
					aluOE = 1'b0;
					opImm = 1'b0;
					valueFormerOE = 1'b0;
					valueFormerOperation = 1'b0;
					halt = 1'b0;
					
					registerFileWE = 1'b0;//can't latch until second posedge
					memoryEnable = 1'b1;//Enable memory
					memoryOperation = 2'b00;//load instruction
					
					//Cannot increment pc yet
					pcWE = 1'b0;
					pcALUOE = 1'b0;
					pcALUOperation = 2'b00;
				end
				7'b0100011://memory write
				begin//This happens first (for sh and sb)
					registerFileWE = 1'b0;
					aluOE = 1'b0;
					opImm = 1'b0;
					valueFormerOE = 1'b0;
					valueFormerOperation = 1'b0;
					halt = 1'b0;
					
					memoryEnable = 1'b1;//Enable memory
					
					if (funct3 == 3'b010)//sw instruction
						memoryOperation = 2'b00;//Should not be in STATE_EXECUTE for sw
					else//sb or sh
						memoryOperation = 2'b01;//memory needs to read the old value stored at the address so it can modify it with a new byte/half word first to be written on the next posedge
				
					//Cannot increment pc yet
					pcWE = 1'b0;
					pcALUOE = 1'b0;
					pcALUOperation = 2'b00;
				end
				default://Any other opcode should not need an extra cycle
				begin
					//Disable everything
					registerFileWE = 1'b0;
					aluOE = 1'b0;
					opImm = 1'b0;
					valueFormerOE = 1'b0;
					valueFormerOperation = 1'b0;
					pcWE = 1'b0;
					pcALUOE = 1'b0;
					pcALUOperation = 2'b00;
					memoryEnable = 1'b0;
					memoryOperation = 2'b00;
					
					halt = 1'b1;//Something went wrong
				end
			endcase
		end
		STATE_HALT:
		begin
			//Leave all outputs and registers as is (don't enable anything)
			registerFileWE = 1'b0;
			aluOE = 1'b0;
			opImm = 1'b0;
			valueFormerOE = 1'b0;
			valueFormerOperation = 1'b0;
			pcWE = 1'b0;
			pcALUOE = 1'b0;
			pcALUOperation = 2'b00;
			memoryEnable = 1'b0;
			memoryOperation = 2'b00;
			
			halt = 1'b1;//There is no escaping the halt state, but leave this as 1 anyways
		end
	endcase
end

endmodule