/* Endianness functions */
//todo add functions for halfwords

function automatic [31:0] toLittleEndian(input [31:0] dataIn);
begin
	toLittleEndian = swapEndianness(dataIn);
end
endfunction

function automatic [31:0] toBigEndian(input [31:0] dataIn);
begin
	toBigEndian = swapEndianness(dataIn);
end
endfunction

function automatic [15:0] toLittleEndian16(input [15:0] dataIn);
begin
	toLittleEndian16 = swapEndianness16(dataIn);
end
endfunction

function automatic [15:0] toBigEndian16(input [15:0] dataIn);
begin
	toBigEndian16 = swapEndianness16(dataIn);
end
endfunction

function automatic [31:0] swapEndianness(input [31:0] dataIn);
begin
	swapEndianness = {dataIn[7:0], dataIn[15:8], dataIn[23:16], dataIn[31:24]};
end
endfunction 

function automatic [15:0] swapEndianness16(input [15:0] dataIn);
begin
	swapEndianness16 = {dataIn[7:0], dataIn[15:8]};
end
endfunction 