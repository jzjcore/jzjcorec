//I wrote this in the past while learning about fpgas. Probably only works on fpgas because of readmemb/readmemh (but I doubt anyone will make an asic with this cpu anyways)
module sync_ram//dual port
(
	input clock,
	
	input [D_MAX:0] data_in,
	input [A_MAX:0] write_address,
	input write_enable,
	
	output reg [D_MAX:0] data_out,
	input [A_MAX:0] read_address
);
//based on https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/hb/qts/qts_qii51007.pdf

parameter INITIALIZE_FROM_FILE = 0;//whether to have default ram contents at boot
parameter FILE = "rom.mem";
parameter FILE_TYPE_BIN = 0;//hex by default
parameter D_WIDTH = 8;
parameter A_WIDTH = 8;

localparam D_MAX = D_WIDTH - 1;
localparam A_MAX = A_WIDTH - 1;
localparam NUM_OF_ADDRESSES = 2 ** A_WIDTH;
localparam NUM_OF_ADDRESSES_MAX = NUM_OF_ADDRESSES - 1;

reg [D_MAX:0] ram [NUM_OF_ADDRESSES_MAX:0];

initial
begin
	if (INITIALIZE_FROM_FILE)
	begin
		if (FILE_TYPE_BIN)
		  $readmemb(FILE, ram);
		else
		  $readmemh(FILE, ram);
	end
end

always @(posedge clock)
begin
	if (write_enable)
		ram[write_address] <= data_in;
	
	data_out <= ram[read_address];//will have old values until next clock cycle
end

endmodule